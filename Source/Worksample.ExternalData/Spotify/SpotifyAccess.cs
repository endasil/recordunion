﻿using AutoMapper;
using SpotifyAPI.Web;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Worksample.Common.Exceptions;
using Worksample.Common.Interfaces;
using Worksample.Common.Models;
using Worksample.ExternalData.Spotify.Settings;

namespace Worksample.ExternalData.Spotify
{
    public class SpotifyAccess : IArtistAccess
    {
        private readonly IMapper _mapper;

        // Use a static instance of spotifyClient to avoid having to recreate it.
        private static SpotifyClient? _spotifyClient;

        public SpotifyAccess(IOptions<SpotifyAccessSettings> spotifyAccessSettings, IMapper mapper)
        {
            _mapper = mapper;

            // Use only one static instance of _spotifyClient. The reason for this is that
            // every time new spotifyClientConfig is created, a new instance of HttpClient
            // is instantiated and that can be bad for performance according to the
            // documentation.
            if (_spotifyClient == null)
            {
                var config = SpotifyClientConfig
                    .CreateDefault()
                    .WithAuthenticator(new ClientCredentialsAuthenticator(
                        spotifyAccessSettings.Value.ClientId!,
                        spotifyAccessSettings.Value.ClientSecret!));
                _spotifyClient = new SpotifyClient(config);
            }
        }

        public async Task<Artist> GetArtistByName(string name)
        {

            // Send a search to spotify search endpoint for entities of type Artist and the name provided.
            var searchResponse =
                await _spotifyClient!.Search.Item(new SearchRequest(SearchRequest.Types.Artist, name));
            
            // If more than one response is received, just grab the first one.
            var artist = searchResponse.Artists.Items!.FirstOrDefault();
            if (artist == null)
            {
                throw new ArtistNotFoundException("No such artist");
            }

            // Convert this entity from the spotify format to the object we want to return from the app.
            // This way it is possible to have a generic interface for getting artists and have
            // each implementation translate it to our common format.
            return _mapper.Map<FullArtist, Artist>(artist);
        }
    }
}
