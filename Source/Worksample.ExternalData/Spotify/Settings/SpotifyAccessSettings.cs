﻿namespace Worksample.ExternalData.Spotify.Settings
{
    public class SpotifyAccessSettings
    {
        public string? ClientId { get; set; }
        public string? ClientSecret { get; set; }
    }
}
