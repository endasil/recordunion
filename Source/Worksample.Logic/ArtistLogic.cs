﻿using System.Threading.Tasks;
using Worksample.Common.Helper;
using Worksample.Common.Interfaces;
using Worksample.Common.Models;

namespace Worksample.Logic
{
    public class ArtistLogic : IArtistLogic
    {
        private readonly IArtistAccess _artistAccess;

        public ArtistLogic(IArtistAccess artistAccess)
        {
            _artistAccess = artistAccess;
        }

        public async Task<Artist> GetArtistByName(string name)
        {
            // Normalize the format of the name string and then pass it on to the data layer
            return await _artistAccess.GetArtistByName(Utils.NormalizeSeachString(name));
        }
    }
}
