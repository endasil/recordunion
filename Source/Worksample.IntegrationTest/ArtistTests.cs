﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Worksample.Common.Models;
using Worksample.Service;
using Xunit;
using static Xunit.Assert;


// Run integration tests bootstrapping the application in memory using the Startup class
// in the Worksample.Service project.
public class WorksampleIntegrationTests : IClassFixture<WebApplicationFactory<Startup>>
{
    readonly HttpClient _client;

    public WorksampleIntegrationTests(WebApplicationFactory<Startup> fixture)
    {
        _client = fixture.CreateClient();
    }
    // Test a case when the 
    [Theory]
    [InlineData("/api/Artist/KaTy.PeRrY")]
    public async Task Get_KatyPerry_With_Dot_Success(string url)
    {
        var response = await _client.GetAsync(url);
        
        Equal(HttpStatusCode.OK, response.StatusCode);

        var artist = JsonConvert.DeserializeObject<Artist>(await response.Content.ReadAsStringAsync());
        Equal("Katy Perry", artist.ArtistName );
        Equal("6jJ0s89eD6GaHleKKya26X",artist.ArtistId);
    }

    [Theory]
    [InlineData("/api/Artist/TheKatyPerry")]

    public async Task Get_TheKatyPerry__NotFound(string url)
    {
        var response = await _client.GetAsync(url);
        Equal(HttpStatusCode.NotFound, response.StatusCode);

    }

}