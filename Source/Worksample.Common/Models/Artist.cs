﻿namespace Worksample.Common.Models
{
    public class Artist
    {
        public string ArtistName { get; set; } = null!;
        public string ArtistId { get; set; } = null!;
    }
}
