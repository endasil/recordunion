﻿using System.Text.RegularExpressions;

namespace Worksample.Common.Helper
{
    public class Utils
    {

        /// <summary>
        /// Normalize the string according to the rules we have for search strings.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string NormalizeSeachString(string input)
        {
            return Regex.Replace(input, "[. ]", "").ToLower();
        }
    }
}
