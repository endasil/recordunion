﻿using System.Threading.Tasks;
using Worksample.Common.Models;

namespace Worksample.Common.Interfaces
{
    public interface IArtistLogic
    {
        public Task<Artist> GetArtistByName(string name);
    }
}
