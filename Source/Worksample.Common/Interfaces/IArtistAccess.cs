﻿using System.Threading.Tasks;
using Worksample.Common.Models;

namespace Worksample.Common.Interfaces
{
    public interface IArtistAccess
    {
        Task<Artist> GetArtistByName(string name);
    }
}