using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Worksample.Common.Interfaces;
using Worksample.ExternalData.Spotify;
using Worksample.ExternalData.Spotify.Settings;
using Worksample.Logic;

namespace Worksample.Service
{
    public class Startup
    { 
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            
            // Add support for swagger documentation
            services.AddSwaggerGen(c =>
            {
                c.EnableAnnotations();
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Worksample.Service", Version = "v1" });
            });
            services.AddOptions();

            // Add automapper, a tool for making it easier to transfer data between different models.
            services.AddAutoMapper(typeof(Startup));

            // Load settings from the config file into a SpotifySettings class. This allows for getting
            // strongly typed access to similar data grouped by a class. 
            services.Configure<SpotifyAccessSettings>(options
                => Configuration.GetSection("SpotifySettings").Bind(options));

            // Register services. Using transient meaning that a new instance will be created for each service referencing
            // it. 
            services.AddTransient<IArtistLogic, ArtistLogic>();
            services.AddTransient<IArtistAccess, SpotifyAccess>();
        }


        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                string swaggerJsonBasePath = string.IsNullOrWhiteSpace(c.RoutePrefix) ? "." : "..";
                c.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/v1/swagger.json", "My API");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
