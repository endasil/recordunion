﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.Annotations;
using Worksample.Common.Exceptions;
using Worksample.Common.Models;
using Worksample.Common.Interfaces;

namespace Worksample.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArtistController : ControllerBase
    {
        private readonly IArtistLogic _artistLogic;

        public ArtistController(IArtistLogic artistLogic)
        {
            _artistLogic = artistLogic;
        }

        [HttpGet("{name}")]
        [SwaggerOperation(Summary = "Search for an artist based on name.", Description = "Search for an artist based on name and returns the id and name.")]
        [SwaggerResponse(200, null, Description = "It's all good!")]
        [SwaggerResponse(404, null, Description = "Artist was not found.")]
        [SwaggerResponse(500, null, Description = "Unexpected error.")]
        public async Task<ActionResult<Artist>> GetArtistByName(string name)
        {
            try
            {
                var artist = await _artistLogic.GetArtistByName(name);
                return Ok(artist);
            }
            catch (ArtistNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
