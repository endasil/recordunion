﻿using System;
using AutoMapper;
using SpotifyAPI.Web;
using Worksample.Common.Models;

namespace Worksample.Service.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<FullArtist, Artist>()
                .ForMember(model => model.ArtistId,
                    opt => opt.MapFrom(m => m.Id))
                .ForMember(model => model.ArtistName,
                opt => opt.MapFrom(m => m.Name));


        }
    }
}
