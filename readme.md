

### Running the application

If you start the application through visual studio, make sure that Worksample.Service is set as the startup project. Then just run it and a browser page should automatically open with the URL to for swagger. If you run it by starting the exe file, the swagger documentation should be found at https://localhost:5001/swagger/index.html. To get information about an artist, simply make a call to https://localhost:5001/api/Artist/{artistName}, for example, https://localhost:5001/api/Artist/Katy.Perry

### Structure

The application consists of 3 main layers for handling the flow of requests, **Service** , **Logic,** and **ExternalData**. The **Service** layer contains the entry point and actual service taking API calls. The **Logic layer** is the pace for performing various business logic. **ExternalData** is a layer for retrieving data from external endpoints. There is also a **Common** layer for common data used by all layers and an **IntegrationTest** layer responsible for running integration tests.

![Structure.png](Structure.png) 



**Common layer**

The **Common** project is where I keep information that is to be accessible from all layers. I keep Interfaces here to have them as a separate thing from the layer that actually implements it. If we take the example of *IArtistAccess*, the implementation of this is in the project *ExternalData*, but if I in the future would like to add a new project called *InternalData* and get artist information from an internal service, I can just change the interface <-> service registration in *startup.cs*, and the Logic Layer will automatically be able to get data from this new source without any changes there. I also keep Models here that is to be accessible to the rest of the application. There may be some cases when you want to have a Model for the business layer and another that is used as a data source for the client to generate its display, but in this case, there has not been any need for such distinction.  Other than that I keep generic helper functions here as well as application-specific exceptions.

![CommonLayerStructure.png](CommonLayerStructure.png) )  



The *Artist.cs* file here represents the model we want to return from the app. The *IArtistAccess* interface class is used to define a blueprint for how classes that get artist data are called and that they should translate whatever data they get into the Artist class format. By using a generic interface like this the caller to the data class does not have to care about if the data comes from Spotify, iTunes, or any other service, the returned data will be the same.

![ArtistFile.png](ArtistFile.png) 



![IArtistAccessFile.png](IArtistAccessFile.png) 



**Service Layer**

This is the project containing the startup code and is the entry point for the service. Files of interest here where I have made changes are the *Startup.cs* file where I register connections between various services and their interfaces, *MappingProfile.cs* where I specify instructions for how to map data between different domain models with *AutoMapper,* and then we have the *ArtistController.cs* file that receives requests for getting an artist.



![ServiceLayerStructure](ServiceLayerStructure.png) 

 

In the *startup.cs* file, configuration settings for Spotify are loaded from the *appsettings.json* file into a class called *SpotifySettings*. This way similar configuration data is grouped together under a class and classes and it is easy for other classes to be dependent on only the configuration they need.

I choose to create all service bindings as *Transient*, the idea is that everything is stateless unless there is an explicit need for it not to be. If there was a need to caching in memory, for example, loading the service as a singleton would make sense, while if I needed a class to maintain its state through a call, I would register it as a *Scoped* instead.

![StartupFile](StartupFile.png)



In the artist controller file, I pass in a reference to the service registered for *ArtistLogic* in the *service.cs* file through dependency injection in the constructor. The *GetArtistByName* function receives requests for artist information and passes it on to a function in the Logic layer. It is also responsible for catching exceptions and returning the right error code.



![](ArtistController.png)





**Logic Layer**

This project consists of only one file, *ArtistLogic.cs*



![LogicLayerStructure.png](LogicLayerStructure.png) 



The *ArtistLogic* takes in a reference to an *artistAccess* class by an *IArtistAccess* Interface, using whatever implementation was registered in the *Startup.cs* file. The *GetArtistByName* function will normalize the serachString format (remove all space and .) with the help of a static helped class and a function. It will then go on and pass the call to the data layer where we get the actual data.



 ![IArtistLogic.png](IArtistLogic.png) 



**ExternalData Layer**

This project consists of two files, *SpotifyAccessSettings.cs* containing the *ClientId* and *ClientSecret* needed to connect to Spotify. It also contains the *SpotifyAccess.cs* file where the logic needed to retrieve data from Spotify resides.



![ExternalAccessLayerStructure.png](ExternalAccessLayerStructure.png) 



The *SpotifyAccess* class implements the IArtistAcces* interface defined in the **Common Layer**.

The *GetArtistByName* function makes a call to Spotify using the normalized version of the "name" string sent in from the *GetArtistByName* function in the previous layer. It will then map the response from the Spotify specific data class to the version of the class containing the specific data we need and return it. The mapping ensures that we can use the same interface regardless of implementation.



![SpotifyAccessTop.png](SpotifyAccessTop.png)



![SpotifyAccessGetArtistByName.png](SpotifyAccessGetArtistByName.png)



